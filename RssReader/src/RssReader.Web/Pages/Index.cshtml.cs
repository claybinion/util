using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Toolkit.Parsers.Rss;
using RssReader.Web.Services.Interfaces;

namespace RssReader.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IRssService rssService;

        public IndexModel(IRssService rssService)
        {
            this.rssService = rssService;
        }
        public List<string> TitleList { get; set; } = new List<string>();
        public List<string> SummaryList { get; set; } = new List<string>();
        public List<string> ContentList { get; set; } = new List<string>();

        public async Task OnGet()
        {
            //SyndicationFeed feed = rssService.GetRssFeed(null);
            string feedUrl = "https://daringfireball.net/feeds/main";
            //    //feedUrl = "https://xkcd.com/rss.xml";

            IEnumerable<RssSchema> feed = await rssService.GetAndParseRSS(feedUrl);

            foreach (var item in feed)
            {
                if (item.Content != null)
                {
                    TitleList.Add(item.Title);    
                }

                if (item.Content != null)
                {
                    ContentList.Add(item.Content);
                }

                else if (item.Summary != null)
                {
                    SummaryList.Add(item.Summary);
                }
            }
        }
    }
}
