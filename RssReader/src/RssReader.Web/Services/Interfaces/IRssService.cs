﻿using Microsoft.Toolkit.Parsers.Rss;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;

namespace RssReader.Web.Services.Interfaces
{
    public interface IRssService
    {
        public Task<string> GetRssFeed(string feedUrl);
        public Task<IEnumerable<RssSchema>> GetAndParseRSS(string feedUrl);
    }
}
