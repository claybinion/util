﻿using Microsoft.Toolkit.Parsers.Rss;
using RssReader.Web.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Xml;

namespace RssReader.Web.Services
{
    public class RssService : IRssService
    {
        //public SyndicationFeed GetRssFeed(string feedUrl)
        //{
        //    feedUrl = "https://daringfireball.net/feeds/main";
        //    //feedUrl = "https://xkcd.com/rss.xml";

        //    XmlReader reader = XmlReader.Create(feedUrl);
        //    SyndicationFeed feed = SyndicationFeed.Load(reader);
        //    reader.Close();

        //    return feed;
        //}

        public async Task<string> GetRssFeed(string feedUrl)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var feed = await client.GetStringAsync(feedUrl);
                    return feed;
                }
                catch (Exception ex)
                {}
            }

            return null;
        }

        public async Task<IEnumerable<RssSchema>> GetAndParseRSS(string feedUrl)
        {
            var feed = await GetRssFeed(feedUrl);

            if (feed != null)
            {
                var parser = new RssParser();
                IEnumerable<RssSchema> rss = parser.Parse(feed);

                return rss;
            }

            return null;
        }
    }
}
