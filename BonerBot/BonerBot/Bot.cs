﻿using Discord;
using Discord.WebSocket;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace BonerBot
{
    public class Bot
    {

        public static void Main(string[] args)
        => new Bot().MainAsync().GetAwaiter().GetResult();

        private DiscordSocketClient _client;
        Jammer jammer = new Jammer();

        public async Task MainAsync()
        {
            _client = new DiscordSocketClient();

            _client.Log += Log;

            await _client.LoginAsync(TokenType.Bot,
                Environment.GetEnvironmentVariable("DiscordToken"));
            await _client.StartAsync();

            ulong channelId = 729519550281941073;

            var channel = _client.PrivateChannels.Where(x => x.Id == channelId);

            _client.MessageReceived += MessageReceivedAsync;

            // Block this task until the program is closed.
            await Task.Delay(-1);
        }

        private async Task MessageReceivedAsync(SocketMessage message)
        {
            // TODO: https://discord.foxbot.me/docs/faq/commands/general.html?tabs=cmdattrib
            // The bot should never respond to itself.
            if (message.Author.Id == _client.CurrentUser.Id)
                return;

            if (message.Content.StartsWith("testes"))
            {
                await message.Channel.SendMessageAsync($"https://media.giphy.com/media/AxpvyWYDHuIH6/giphy.gif");
            }

            if (message.Content.StartsWith("https://open.spotify.com/track/"))
            {
                var msgContent = message.Content;
                
                await jammer.Start();
                await jammer.AddSong(msgContent);

                await message.Channel.SendMessageAsync($"The song was added");
            }
        }

        public Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
