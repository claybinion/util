﻿using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BonerBot
{
    public class Jammer
    {
        private static readonly string clientId = Environment.GetEnvironmentVariable("SPOTIFY_CLIENT_ID");
        private static readonly string clientSecret = Environment.GetEnvironmentVariable("SPOTIFY_CLIENT_SECRET");

        private static SpotifyWebAPI _spotify;

        // TODO: We need to fix how auth works. And how it will work on the server that will run this
        //  It errors out when I am not logged in first. And the localhost stuff probably will need to be fixed for the server.
        public async Task Start()
        {
            ImplicitGrantAuth auth = new ImplicitGrantAuth(
                clientId,
                "https://bonerbot20200710230315.azurewebsites.net",
                "https://bonerbot20200710230315.azurewebsites.net",
                Scope.UserReadPrivate
              );
            auth.AuthReceived += async (sender, payload) =>
            {
                auth.Stop(); // `sender` is also the auth instance
                _spotify = new SpotifyWebAPI()
                {
                    TokenType = payload.TokenType,
                    AccessToken = payload.AccessToken
                };
                // Do requests with API client
            };
            auth.Start(); // Starts an internal HTTP Server
            auth.OpenBrowser();

            await Task.Delay(500);
        }

        public async Task<string> AddSong(string fullTrackUrl)
        {
            var trackUri = ExtractUri(fullTrackUrl);

            string bonerJamsPlaylistId = "5D9fjqz5tzOf1pVoaUtZsh";

            var bonerInfo = await _spotify.GetTrackAsync(trackUri);
            var newBonerJam = await _spotify.AddPlaylistTrackAsync(bonerJamsPlaylistId, trackUri);

            if (newBonerJam.HasError())
            {
                Console.WriteLine("Error Status: " + newBonerJam.Error.Status);
                Console.WriteLine("Error Msg: " + newBonerJam.Error.Message);
            }

            var bonerJams = await _spotify.GetPlaylistTracksAsync(bonerJamsPlaylistId);

            foreach (PlaylistTrack jamPage in bonerJams.Items)
            {
                if (jamPage.Track is FullTrack track)
                {
                    Console.WriteLine(track.Name);
                }
            }

            return bonerInfo.Name;
        }

        public string ExtractUri(string fullUrl)
        {
            int beginTrim = fullUrl.IndexOf("https://open.spotify.com/track/") + "https://open.spotify.com/track/".Length;
            int endTrim = fullUrl.LastIndexOf("?si=");
            String trackId = fullUrl.Substring(beginTrim, endTrim - beginTrim);

            return "spotify:track:" + trackId;
        }
    }
}
