﻿using SpotifyAPI;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using SpotifyAPI.Web.Models;
using System.Threading.Tasks;
using System.Linq;

namespace Jammer
{
    public class Spotify
    {
        //private const string CredentialsPath = "credentials.json";
        private static readonly string clientId = Environment.GetEnvironmentVariable("SPOTIFY_CLIENT_ID");
        private static readonly string clientSecret = Environment.GetEnvironmentVariable("SPOTIFY_CLIENT_SECRET");
        //private static readonly EmbedIOAuthServer _server = new EmbedIOAuthServer(new Uri("http://localhost:4002/callback"), 4002);

        private static SpotifyWebAPI _spotify;

        public static void Main(string[] args)
        => new Spotify().MainAsync(args).GetAwaiter().GetResult();

        public async Task MainAsync(string[] args)
        {
            ImplicitGrantAuth auth = new ImplicitGrantAuth(
                clientId,
                "http://localhost:4002",
                "http://localhost:4002",
                Scope.UserReadPrivate
              );
            auth.AuthReceived += async (sender, payload) =>
            {
                auth.Stop(); // `sender` is also the auth instance
                _spotify = new SpotifyWebAPI()
                {
                    TokenType = payload.TokenType,
                    AccessToken = payload.AccessToken
                };
                // Do requests with API client
            };
            auth.Start(); // Starts an internal HTTP Server
            auth.OpenBrowser();

            await Task.Delay(500);

            var trackUri = ExtractUri(args.FirstOrDefault());
            await AddSong(trackUri);
        }

        private async Task AddSong(string song)
        {
            string bonerJamsId = "5D9fjqz5tzOf1pVoaUtZsh";
            
            var bonerJams = await _spotify.GetPlaylistTracksAsync(bonerJamsId);
            var addTrack = await _spotify.AddPlaylistTrackAsync(bonerJamsId, song);

            if (addTrack.HasError())
            {
                Console.WriteLine("Error Status: " + addTrack.Error.Status);
                Console.WriteLine("Error Msg: " + addTrack.Error.Message);
            }

            foreach (PlaylistTrack jamPage in bonerJams.Items)
            {
                if (jamPage.Track is FullTrack track)
                {
                    Console.WriteLine(track.Name);
                }
            }
        }

        public string ExtractUri(string fullUrl)
        {
            int beginTrim = fullUrl.IndexOf("https://open.spotify.com/track/") + "https://open.spotify.com/track/".Length;
            int endTrim = fullUrl.LastIndexOf("?si=");
            String trackId = fullUrl.Substring(beginTrim, endTrim - beginTrim);

            return "spotify:track:" + trackId;
        }

        //private SpotifyClient _spotifyClient;


        //public async Task<int> StartSpotifyClient()
        //{
        //    if (string.IsNullOrEmpty(clientId) || string.IsNullOrEmpty(clientSecret))
        //    {
        //        throw new NullReferenceException(
        //          "Please set SPOTIFY_CLIENT_ID and SPOTIFY_CLIENT_SECRET via environment variables before starting the program"
        //        );
        //    }

        //    if (File.Exists(CredentialsPath))
        //    {
        //        await Start();
        //    }
        //    else
        //    {
        //        await StartAuthentication();
        //    }

        //    Console.ReadKey();

        //    return 0;
        //}

        //public async Task Start()
        //{
        //    var json = await File.ReadAllTextAsync(CredentialsPath);
        //    var token = JsonConvert.DeserializeObject<AuthorizationCodeTokenResponse>(json);

        //    var authenticator = new AuthorizationCodeAuthenticator(clientId!, clientSecret!, token);
        //    authenticator.TokenRefreshed += (sender, token) => File.WriteAllText(CredentialsPath, JsonConvert.SerializeObject(token));

        //    var config = SpotifyClientConfig.CreateDefault()
        //      .WithAuthenticator(authenticator);

        //    _spotifyClient = new SpotifyClient(config);

        //    //int beginTrim = trackUrl.IndexOf("https://open.spotify.com/track/") + "https://open.spotify.com/track/".Length;
        //    //int endTrim = trackUrl.LastIndexOf("?si=");
        //    //String trackId = trackUrl.Substring(beginTrim, endTrim - beginTrim);

        //    //string bonerJamsId = "5D9fjqz5tzOf1pVoaUtZsh";

        //    //var song = new List<string> { "spotify:track:" + trackId };
        //    //var songRequest = new PlaylistAddItemsRequest(song);
        //    //var addSong = await _spotifyClient.Playlists.AddItems(bonerJamsId, songRequest);

        //    //var bonerJams = await _spotifyClient.Playlists.GetItems(bonerJamsId);

        //    //foreach (PlaylistTrack<IPlayableItem> jam in bonerJams.Items)
        //    //{
        //    //    if (jam.Track is FullTrack track)
        //    //    {
        //    //        Console.WriteLine(track.Name);
        //    //    }
        //    //}
        //}

        //private async Task AddSong(string trackUrl)
        //{
        //    int beginTrim = trackUrl.IndexOf("https://open.spotify.com/track/") + "https://open.spotify.com/track/".Length;
        //    int endTrim = trackUrl.LastIndexOf("?si=");
        //    String trackId = trackUrl.Substring(beginTrim, endTrim - beginTrim);

        //    // https://open.spotify.com/track/4tv5LzVrftOVRhMR8yxbi6?si=oD8231xfTeK8tDinFLocXA
        //    trackUrl.Remove(31);

        //    //var playlists = await _spotifyClient.PaginateAll(await _spotifyClient.Playlists.CurrentUsers().ConfigureAwait(false));

        //    string bonerJamsId = "5D9fjqz5tzOf1pVoaUtZsh";


        //    // Get a list of the posts from discord
        //    // Cycle through them, check for duplicates?
        //    //      Seems like this can work more efficiently than cycling through alll the discord posts, 
        //    //      check dates? save the last one and continue from that date next time this runs?
        //    // Strip out the 'spotify:track:uri' from the tracks
        //    // Add each of the new tracks to the songList
        //    // ? Display all the new songs added?
        //    // 

        //    // New song   2SDKPv9mCbo701ajC3LWFa
        //    var songList = new List<string> { "spotify:track:" + trackId };
        //    var songRequest = new PlaylistAddItemsRequest(songList);
        //    var addSong = await _spotifyClient.Playlists.AddItems(bonerJamsId, songRequest);

        //    var bonerJams = await _spotifyClient.Playlists.GetItems(bonerJamsId);

        //    foreach (PlaylistTrack<IPlayableItem> jam in bonerJams.Items)
        //    {
        //        if (jam.Track is FullTrack track)
        //        {
        //            Console.WriteLine(track.Name);
        //        }
        //    }

        //    //foreach (var playlist in playlists)
        //    //{
        //    //    //Console.WriteLine($"Playlist Name: { playlist.Name }");

        //    //    if (playlist.Name.StartsWith("B"))
        //    //    {
        //    //        Console.WriteLine($"Playlist Name: { playlist.Name }");
        //    //    }
        //    //}

        //    _server.Dispose();
        //}

        //// For Spotify
        //private async Task StartAuthentication()
        //{
        //    await _server.Start();
        //    _server.AuthorizationCodeReceived += OnAuthorizationCodeReceived;

        //    var request = new LoginRequest(_server.BaseUri, clientId!, LoginRequest.ResponseType.Code)
        //    {
        //        Scope = new List<string> { UserReadEmail, UserReadPrivate, PlaylistReadPrivate, PlaylistModifyPublic, PlaylistModifyPrivate, PlaylistReadCollaborative }
        //    };

        //    Uri uri = request.ToUri();
        //    try
        //    {
        //        BrowserUtil.Open(uri);
        //    }
        //    catch (Exception)
        //    {
        //        Console.WriteLine("Unable to open URL, manually open: {0}", uri);
        //    }
        //}

        //// For Spotify
        //private async Task OnAuthorizationCodeReceived(object sender, AuthorizationCodeResponse response)
        //{
        //    await _server.Stop();
        //    AuthorizationCodeTokenResponse token = await new OAuthClient().RequestToken(
        //      new AuthorizationCodeTokenRequest(clientId!, clientSecret!, response.Code, _server.BaseUri)
        //    );

        //    await File.WriteAllTextAsync(CredentialsPath, JsonConvert.SerializeObject(token));
        //    await Start();
        //}

    }
}
